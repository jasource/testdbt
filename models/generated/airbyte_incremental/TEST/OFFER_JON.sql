{{ config(
    cluster_by = ["_AIRBYTE_UNIQUE_KEY", "_AIRBYTE_EMITTED_AT"],
    unique_key = "_AIRBYTE_UNIQUE_KEY",
    schema = "TEST",
    tags = [ "top-level" ]
) }}
-- Final base SQL model
-- depends_on: {{ ref('OFFER_JON_SCD') }}
select
    _AIRBYTE_UNIQUE_KEY,
    ID,
    MPN,
    MPQ,
    EXPIRY,
    CHECKSUM,
    LOCATION,
    QUANTITY,
    DATE_CODE,
    LEAD_TIME,
    VENDOR_ID,
    CREATED_AT,
    EXPIRED_AT,
    OFFER_TYPE,
    UPDATED_AT,
    PRICE_TIERS,
    PRODUCT_SKU,
    AVAILABILITY,
    MANUFACTURER,
    PACKAGING_TYPE,
    SELLABLE_ERRORS,
    COUNTRY_OF_ORIGIN,
    PACKAGING_CONDITION,
    REPLACEMENT_OFFER_ID,
    DATE_CODE_WITHIN_2_YEARS,
    LAST_STOCK_TRANSACTION_ID,
    LAST_STOCK_TRANSACTION_BALANCE,
    _AIRBYTE_AB_ID,
    _AIRBYTE_EMITTED_AT,
    {{ current_timestamp() }} as _AIRBYTE_NORMALIZED_AT,
    _AIRBYTE_OFFER_JON_HASHID
from {{ ref('OFFER_JON_SCD') }}
-- OFFER_JON from {{ source('TEST', '_AIRBYTE_RAW_OFFER_JON') }}
where 1 = 1
and _AIRBYTE_ACTIVE_ROW = 1
{{ incremental_clause('_AIRBYTE_EMITTED_AT') }}

