{{ config(
    cluster_by = ["_AIRBYTE_ACTIVE_ROW", "_AIRBYTE_UNIQUE_KEY_SCD", "_AIRBYTE_EMITTED_AT"],
    unique_key = "_AIRBYTE_UNIQUE_KEY_SCD",
    schema = "TEST",
    post_hook = ["drop view _AIRBYTE_TEST.OFFER_JON_STG"],
    tags = [ "top-level" ]
) }}
-- depends_on: ref('OFFER_JON_STG')
with
{% if is_incremental() %}
new_data as (
    -- retrieve incremental "new" data
    select
        *
    from {{ ref('OFFER_JON_STG')  }}
    -- OFFER_JON from {{ source('TEST', '_AIRBYTE_RAW_OFFER_JON') }}
    where 1 = 1
    {{ incremental_clause('_AIRBYTE_EMITTED_AT') }}
),
new_data_ids as (
    -- build a subset of _AIRBYTE_UNIQUE_KEY from rows that are new
    select distinct
        {{ dbt_utils.surrogate_key([
            'ID',
        ]) }} as _AIRBYTE_UNIQUE_KEY
    from new_data
),
empty_new_data as (
    -- build an empty table to only keep the table's column types
    select * from new_data where 1 = 0
),
previous_active_scd_data as (
    -- retrieve "incomplete old" data that needs to be updated with an end date because of new changes
    select
        {{ star_intersect(ref('OFFER_JON_STG'), this, from_alias='inc_data', intersect_alias='this_data') }}
    from {{ this }} as this_data
    -- make a join with new_data using primary key to filter active data that need to be updated only
    join new_data_ids on this_data._AIRBYTE_UNIQUE_KEY = new_data_ids._AIRBYTE_UNIQUE_KEY
    -- force left join to NULL values (we just need to transfer column types only for the star_intersect macro on schema changes)
    left join empty_new_data as inc_data on this_data._AIRBYTE_AB_ID = inc_data._AIRBYTE_AB_ID
    where _AIRBYTE_ACTIVE_ROW = 1
),
input_data as (
    select {{ dbt_utils.star(ref('OFFER_JON_STG')) }} from new_data
    union all
    select {{ dbt_utils.star(ref('OFFER_JON_STG')) }} from previous_active_scd_data
),
{% else %}
input_data as (
    select *
    from {{ ref('OFFER_JON_STG')  }}
    -- OFFER_JON from {{ source('TEST', '_AIRBYTE_RAW_OFFER_JON') }}
),
{% endif %}
scd_data as (
    -- SQL model to build a Type 2 Slowly Changing Dimension (SCD) table for each record identified by their primary key
    select
      {{ dbt_utils.surrogate_key([
      'ID',
      ]) }} as _AIRBYTE_UNIQUE_KEY,
      ID,
      MPN,
      MPQ,
      EXPIRY,
      CHECKSUM,
      LOCATION,
      QUANTITY,
      DATE_CODE,
      LEAD_TIME,
      VENDOR_ID,
      CREATED_AT,
      EXPIRED_AT,
      OFFER_TYPE,
      UPDATED_AT,
      PRICE_TIERS,
      PRODUCT_SKU,
      AVAILABILITY,
      MANUFACTURER,
      PACKAGING_TYPE,
      SELLABLE_ERRORS,
      COUNTRY_OF_ORIGIN,
      PACKAGING_CONDITION,
      REPLACEMENT_OFFER_ID,
      DATE_CODE_WITHIN_2_YEARS,
      LAST_STOCK_TRANSACTION_ID,
      LAST_STOCK_TRANSACTION_BALANCE,
      UPDATED_AT as _AIRBYTE_START_AT,
      lag(UPDATED_AT) over (
        partition by cast(ID as {{ dbt_utils.type_string() }})
        order by
            UPDATED_AT is null asc,
            UPDATED_AT desc,
            _AIRBYTE_EMITTED_AT desc
      ) as _AIRBYTE_END_AT,
      case when row_number() over (
        partition by cast(ID as {{ dbt_utils.type_string() }})
        order by
            UPDATED_AT is null asc,
            UPDATED_AT desc,
            _AIRBYTE_EMITTED_AT desc
      ) = 1 then 1 else 0 end as _AIRBYTE_ACTIVE_ROW,
      _AIRBYTE_AB_ID,
      _AIRBYTE_EMITTED_AT,
      _AIRBYTE_OFFER_JON_HASHID
    from input_data
),
dedup_data as (
    select
        -- we need to ensure de-duplicated rows for merge/update queries
        -- additionally, we generate a unique key for the scd table
        row_number() over (
            partition by
                _AIRBYTE_UNIQUE_KEY,
                _AIRBYTE_START_AT,
                _AIRBYTE_EMITTED_AT
            order by _AIRBYTE_ACTIVE_ROW desc, _AIRBYTE_AB_ID
        ) as _AIRBYTE_ROW_NUM,
        {{ dbt_utils.surrogate_key([
          '_AIRBYTE_UNIQUE_KEY',
          '_AIRBYTE_START_AT',
          '_AIRBYTE_EMITTED_AT'
        ]) }} as _AIRBYTE_UNIQUE_KEY_SCD,
        scd_data.*
    from scd_data
)
select
    _AIRBYTE_UNIQUE_KEY,
    _AIRBYTE_UNIQUE_KEY_SCD,
    ID,
    MPN,
    MPQ,
    EXPIRY,
    CHECKSUM,
    LOCATION,
    QUANTITY,
    DATE_CODE,
    LEAD_TIME,
    VENDOR_ID,
    CREATED_AT,
    EXPIRED_AT,
    OFFER_TYPE,
    UPDATED_AT,
    PRICE_TIERS,
    PRODUCT_SKU,
    AVAILABILITY,
    MANUFACTURER,
    PACKAGING_TYPE,
    SELLABLE_ERRORS,
    COUNTRY_OF_ORIGIN,
    PACKAGING_CONDITION,
    REPLACEMENT_OFFER_ID,
    DATE_CODE_WITHIN_2_YEARS,
    LAST_STOCK_TRANSACTION_ID,
    LAST_STOCK_TRANSACTION_BALANCE,
    _AIRBYTE_START_AT,
    _AIRBYTE_END_AT,
    _AIRBYTE_ACTIVE_ROW,
    _AIRBYTE_AB_ID,
    _AIRBYTE_EMITTED_AT,
    {{ current_timestamp() }} as _AIRBYTE_NORMALIZED_AT,
    _AIRBYTE_OFFER_JON_HASHID
from dedup_data where _AIRBYTE_ROW_NUM = 1

