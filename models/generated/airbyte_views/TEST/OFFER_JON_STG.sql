{{ config(
    cluster_by = ["_AIRBYTE_EMITTED_AT"],
    unique_key = '_AIRBYTE_AB_ID',
    schema = "_AIRBYTE_TEST",
    tags = [ "top-level-intermediate" ]
) }}
-- SQL model to build a hash column based on the values of this record
-- depends_on: {{ ref('OFFER_JON_AB2') }}
select
    {{ dbt_utils.surrogate_key([
        'ID',
        'MPN',
        'MPQ',
        'EXPIRY',
        'CHECKSUM',
        'LOCATION',
        'QUANTITY',
        'DATE_CODE',
        'LEAD_TIME',
        'VENDOR_ID',
        'CREATED_AT',
        'EXPIRED_AT',
        'OFFER_TYPE',
        'UPDATED_AT',
        'PRICE_TIERS',
        'PRODUCT_SKU',
        'AVAILABILITY',
        'MANUFACTURER',
        'PACKAGING_TYPE',
        'SELLABLE_ERRORS',
        'COUNTRY_OF_ORIGIN',
        'PACKAGING_CONDITION',
        'REPLACEMENT_OFFER_ID',
        boolean_to_string('DATE_CODE_WITHIN_2_YEARS'),
        'LAST_STOCK_TRANSACTION_ID',
        'LAST_STOCK_TRANSACTION_BALANCE',
    ]) }} as _AIRBYTE_OFFER_JON_HASHID,
    tmp.*
from {{ ref('OFFER_JON_AB2') }} tmp
-- OFFER_JON
where 1 = 1
{{ incremental_clause('_AIRBYTE_EMITTED_AT') }}

