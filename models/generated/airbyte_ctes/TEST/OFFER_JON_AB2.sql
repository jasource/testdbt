{{ config(
    cluster_by = ["_AIRBYTE_EMITTED_AT"],
    unique_key = '_AIRBYTE_AB_ID',
    schema = "_AIRBYTE_TEST",
    tags = [ "top-level-intermediate" ]
) }}
-- SQL model to cast each column to its adequate SQL type converted from the JSON schema type
-- depends_on: {{ ref('OFFER_JON_AB1') }}
select
    cast(ID as {{ dbt_utils.type_float() }}) as ID,
    cast(MPN as {{ dbt_utils.type_string() }}) as MPN,
    cast(MPQ as {{ dbt_utils.type_float() }}) as MPQ,
    cast(EXPIRY as {{ dbt_utils.type_string() }}) as EXPIRY,
    cast(CHECKSUM as {{ dbt_utils.type_string() }}) as CHECKSUM,
    cast(LOCATION as {{ dbt_utils.type_string() }}) as LOCATION,
    cast(QUANTITY as {{ dbt_utils.type_float() }}) as QUANTITY,
    cast(DATE_CODE as {{ dbt_utils.type_string() }}) as DATE_CODE,
    cast(LEAD_TIME as {{ dbt_utils.type_float() }}) as LEAD_TIME,
    cast(VENDOR_ID as {{ dbt_utils.type_float() }}) as VENDOR_ID,
    cast(CREATED_AT as {{ dbt_utils.type_string() }}) as CREATED_AT,
    cast(EXPIRED_AT as {{ dbt_utils.type_string() }}) as EXPIRED_AT,
    cast(OFFER_TYPE as {{ dbt_utils.type_string() }}) as OFFER_TYPE,
    cast(UPDATED_AT as {{ dbt_utils.type_string() }}) as UPDATED_AT,
    cast(PRICE_TIERS as {{ dbt_utils.type_string() }}) as PRICE_TIERS,
    cast(PRODUCT_SKU as {{ dbt_utils.type_string() }}) as PRODUCT_SKU,
    cast(AVAILABILITY as {{ dbt_utils.type_string() }}) as AVAILABILITY,
    cast(MANUFACTURER as {{ dbt_utils.type_string() }}) as MANUFACTURER,
    cast(PACKAGING_TYPE as {{ dbt_utils.type_string() }}) as PACKAGING_TYPE,
    cast(SELLABLE_ERRORS as {{ dbt_utils.type_string() }}) as SELLABLE_ERRORS,
    cast(COUNTRY_OF_ORIGIN as {{ dbt_utils.type_string() }}) as COUNTRY_OF_ORIGIN,
    cast(PACKAGING_CONDITION as {{ dbt_utils.type_string() }}) as PACKAGING_CONDITION,
    cast(REPLACEMENT_OFFER_ID as {{ dbt_utils.type_float() }}) as REPLACEMENT_OFFER_ID,
    {{ cast_to_boolean('DATE_CODE_WITHIN_2_YEARS') }} as DATE_CODE_WITHIN_2_YEARS,
    cast(LAST_STOCK_TRANSACTION_ID as {{ dbt_utils.type_float() }}) as LAST_STOCK_TRANSACTION_ID,
    cast(LAST_STOCK_TRANSACTION_BALANCE as {{ dbt_utils.type_float() }}) as LAST_STOCK_TRANSACTION_BALANCE,
    _AIRBYTE_AB_ID,
    _AIRBYTE_EMITTED_AT,
    {{ current_timestamp() }} as _AIRBYTE_NORMALIZED_AT
from {{ ref('OFFER_JON_AB1') }}
-- OFFER_JON
where 1 = 1
{{ incremental_clause('_AIRBYTE_EMITTED_AT') }}

