{{ config(
    cluster_by = ["_AIRBYTE_EMITTED_AT"],
    unique_key = '_AIRBYTE_AB_ID',
    schema = "_AIRBYTE_TEST",
    tags = [ "top-level-intermediate" ]
) }}
-- SQL model to parse JSON blob stored in a single column and extract into separated field columns as described by the JSON Schema
-- depends_on: {{ source('TEST', '_AIRBYTE_RAW_OFFER_JON') }}
select
    {{ json_extract_scalar('_airbyte_data', ['id'], ['id']) }} as ID,
    {{ json_extract_scalar('_airbyte_data', ['mpn'], ['mpn']) }} as MPN,
    {{ json_extract_scalar('_airbyte_data', ['mpq'], ['mpq']) }} as MPQ,
    {{ json_extract_scalar('_airbyte_data', ['expiry'], ['expiry']) }} as EXPIRY,
    {{ json_extract_scalar('_airbyte_data', ['checksum'], ['checksum']) }} as CHECKSUM,
    {{ json_extract_scalar('_airbyte_data', ['location'], ['location']) }} as LOCATION,
    {{ json_extract_scalar('_airbyte_data', ['quantity'], ['quantity']) }} as QUANTITY,
    {{ json_extract_scalar('_airbyte_data', ['date_code'], ['date_code']) }} as DATE_CODE,
    {{ json_extract_scalar('_airbyte_data', ['lead_time'], ['lead_time']) }} as LEAD_TIME,
    {{ json_extract_scalar('_airbyte_data', ['vendor_id'], ['vendor_id']) }} as VENDOR_ID,
    {{ json_extract_scalar('_airbyte_data', ['created_at'], ['created_at']) }} as CREATED_AT,
    {{ json_extract_scalar('_airbyte_data', ['expired_at'], ['expired_at']) }} as EXPIRED_AT,
    {{ json_extract_scalar('_airbyte_data', ['offer_type'], ['offer_type']) }} as OFFER_TYPE,
    {{ json_extract_scalar('_airbyte_data', ['updated_at'], ['updated_at']) }} as UPDATED_AT,
    {{ json_extract_scalar('_airbyte_data', ['price_tiers'], ['price_tiers']) }} as PRICE_TIERS,
    {{ json_extract_scalar('_airbyte_data', ['product_sku'], ['product_sku']) }} as PRODUCT_SKU,
    {{ json_extract_scalar('_airbyte_data', ['availability'], ['availability']) }} as AVAILABILITY,
    {{ json_extract_scalar('_airbyte_data', ['manufacturer'], ['manufacturer']) }} as MANUFACTURER,
    {{ json_extract_scalar('_airbyte_data', ['packaging_type'], ['packaging_type']) }} as PACKAGING_TYPE,
    {{ json_extract_scalar('_airbyte_data', ['sellable_errors'], ['sellable_errors']) }} as SELLABLE_ERRORS,
    {{ json_extract_scalar('_airbyte_data', ['country_of_origin'], ['country_of_origin']) }} as COUNTRY_OF_ORIGIN,
    {{ json_extract_scalar('_airbyte_data', ['packaging_condition'], ['packaging_condition']) }} as PACKAGING_CONDITION,
    {{ json_extract_scalar('_airbyte_data', ['replacement_offer_id'], ['replacement_offer_id']) }} as REPLACEMENT_OFFER_ID,
    {{ json_extract_scalar('_airbyte_data', ['date_code_within_2_years'], ['date_code_within_2_years']) }} as DATE_CODE_WITHIN_2_YEARS,
    {{ json_extract_scalar('_airbyte_data', ['last_stock_transaction_id'], ['last_stock_transaction_id']) }} as LAST_STOCK_TRANSACTION_ID,
    {{ json_extract_scalar('_airbyte_data', ['last_stock_transaction_balance'], ['last_stock_transaction_balance']) }} as LAST_STOCK_TRANSACTION_BALANCE,
    _AIRBYTE_AB_ID,
    _AIRBYTE_EMITTED_AT,
    {{ current_timestamp() }} as _AIRBYTE_NORMALIZED_AT
from {{ source('TEST', '_AIRBYTE_RAW_OFFER_JON') }} as table_alias
-- OFFER_JON
where 1 = 1
{{ incremental_clause('_AIRBYTE_EMITTED_AT') }}

